import random


def perfect_square_discriminant():
    a = 1
    c = random.randrange(4, 20, 2)
    b = c + 1

    print("Trouver le discriminant de {}x² + {}x + {}".format(a, b, c))
    print("Le discriminant est un carré parfait")
    print("Le discriminant est égal à {}".format(compute_discriminant(a, b, c)))

    return a, b, c  # Used for tests


def low_non_square_discriminant():
    a = 1
    b = random.randrange(5, 8)
    c = b

    print("Trouver le discriminant de {}x² + {}x + {}".format(a, b, c))
    print("Le discriminant est un petit nombre (< 30) non carré")
    print("Le discriminant est égal à {}".format(compute_discriminant(a, b, c)))

    return a, b, c  # Used for tests


def high_non_square_discriminant():
    a = 1
    c = random.randrange(6, 20)
    b = random.randrange(c + 2, 20 + 2)

    print("Trouver le discriminant de {}x² + {}x + {}".format(a, b, c))
    print("Le discriminant est un grand nombre (> 30) non carré")
    print("Le discriminant est égal à {}".format(compute_discriminant(a, b, c)))

    return a, b, c  # Used for tests


def negative_discriminant():
    a = 1
    c = random.randrange(4, 20)
    b = random.randrange(1, c - 1)

    while (b**2 >= 4 * c):
        b = random.randrange(1, c - 1)

    print("Trouver le discriminant de {}x² + {}x + {}".format(a, b, c))
    print("Le discriminant est négatif")
    print("Le discriminant est égal à {}".format(compute_discriminant(a, b, c)))

    return a, b, c  # Used for tests


def compute_discriminant(a, b, c):
    return (b**2) - (4 * a * c)
