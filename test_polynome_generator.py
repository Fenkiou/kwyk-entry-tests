import unittest
import math

from polynome_generator import perfect_square_discriminant, \
                               low_non_square_discriminant, \
                               high_non_square_discriminant, \
                               negative_discriminant, compute_discriminant


class TestDiscriminantValues(unittest.TestCase):

    def test_discriminant_is_perfect_square(self):
        a, b, c = perfect_square_discriminant()

        discriminant = compute_discriminant(a, b, c)
        square_root = math.sqrt(discriminant)

        self.assertEqual(square_root**2, discriminant)

    def test_discriminant_is_not_square_and_low(self):
        a, b, c = low_non_square_discriminant()

        discriminant = compute_discriminant(a, b, c)

        self.assertFalse(math.sqrt(discriminant) % 1 == 0)
        self.assertLess(discriminant, 30)

    def test_discriminant_is_not_square_and_high(self):
        a, b, c = high_non_square_discriminant()

        discriminant = compute_discriminant(a, b, c)

        self.assertFalse(math.sqrt(discriminant) % 1 == 0)
        self.assertGreater(discriminant, 30)

    def test_discriminant_is_negative(self):
        a, b, c = negative_discriminant()

        discriminant = compute_discriminant(a, b, c)

        self.assertLess(discriminant, 0)

if __name__ == '__main__':
    unittest.main()
